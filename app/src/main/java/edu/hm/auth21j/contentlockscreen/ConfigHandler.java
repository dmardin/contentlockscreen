package edu.hm.auth21j.contentlockscreen;

import android.support.v7.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigHandler {
    private final static String CONFIG = "config.properties";
    private final static String PASSWORD = "password";
    private final static String DEFAULT_PASSWORD = "ABCD";

    public static String getPasswordPattern() {
        return PASSWORD;
    }

    public static String getProperty(AppCompatActivity layout, String propertyKey) {
        Properties properties = new Properties();
        loadProperties(layout, properties);
        return properties.getProperty(propertyKey);
    }

    public static void setProperty(AppCompatActivity layout, String propertyValue) {
        File file = new File(layout.getFilesDir() + "/" + CONFIG);
        if (file.exists())
            file.delete();
        Properties properties = new Properties();
        loadProperties(layout, properties);
        properties.setProperty(PASSWORD, propertyValue);
        saveProperties(layout, properties);
    }

    public static void createProperty(AppCompatActivity layout) {
        File file = new File(layout.getFilesDir() + "/" + CONFIG);
        if (!file.exists()) {
            Properties properties = new Properties();
            loadProperties(layout, properties);
            properties.setProperty(PASSWORD, DEFAULT_PASSWORD);
            saveProperties(layout, properties);
        }
    }

    private static void loadProperties(AppCompatActivity layout, Properties properties) {
        FileInputStream fi = null;
        try {
            fi = new FileInputStream(layout.getFilesDir() + "/" + CONFIG);
            properties.load(fi);
            fi.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void saveProperties(AppCompatActivity layout, Properties properties) {
        try {
            FileOutputStream fos = layout.openFileOutput(CONFIG, layout.MODE_PRIVATE);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            properties.store(byteArrayOutputStream, "prop");
            fos.write(byteArrayOutputStream.toByteArray());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
