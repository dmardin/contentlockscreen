package edu.hm.auth21j.contentlockscreen;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class ConfigurationLayout extends AppCompatActivity {

    private String sequence = "";
    private TextView passwdView;

    private TextView h1, h2, m1, m2, d1, d2, M1, M2, Y1, Y2, Y3, Y4, bat;
    private char charList[];
    private TextView tvList[];
    private HashMap charMap = new HashMap();

    private final int ANIMATION_DURATION = 1000;
    private final int FADE_IN_DURATION = 200;
    private final int FADE_OUT_DURATION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConfigurationLayout layout = this;
        ConfigHandler.createProperty(layout);
        startService(new Intent(getApplicationContext(), PowerButtonUpdateService.class));
        setContentView(R.layout.layout_configuration);

        initListener();
        charList = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'};
        tvList = new TextView[]{h1, h2, m1, m2, d1, d2, M1, M2, Y1, Y2, Y3, Y4, bat};
        initCharMap();

        passwdView = findViewById(R.id.editTextPasswordPattern);
        loadSequence();
    }

    /**
     * Initializes the onClick listeners for all TextViews and Buttons.
     */
    private void initListener() {

        (h1 = findViewById(R.id.field_h1)).setOnClickListener(view -> addToSequence((TextView)view));
        (h2 = findViewById(R.id.field_h2)).setOnClickListener(view -> addToSequence((TextView)view));
        (m1 = findViewById(R.id.field_m1)).setOnClickListener(view -> addToSequence((TextView)view));
        (m2 = findViewById(R.id.field_m2)).setOnClickListener(view -> addToSequence((TextView)view));
        (d1 = findViewById(R.id.field_d1)).setOnClickListener(view -> addToSequence((TextView)view));
        (d2 = findViewById(R.id.field_d2)).setOnClickListener(view -> addToSequence((TextView)view));
        (M1 = findViewById(R.id.field_M1)).setOnClickListener(view -> addToSequence((TextView)view));
        (M2 = findViewById(R.id.field_M2)).setOnClickListener(view -> addToSequence((TextView)view));
        (Y1 = findViewById(R.id.field_Y1)).setOnClickListener(view -> addToSequence((TextView)view));
        (Y2 = findViewById(R.id.field_Y2)).setOnClickListener(view -> addToSequence((TextView)view));
        (Y3 = findViewById(R.id.field_Y3)).setOnClickListener(view -> addToSequence((TextView)view));
        (Y4 = findViewById(R.id.field_Y4)).setOnClickListener(view -> addToSequence((TextView)view));
        (bat = findViewById(R.id.field_bat)).setOnClickListener(view -> addToSequence((TextView)view));

        final Button playBtn = findViewById(R.id.buttonConfigPlay);
        playBtn.setOnClickListener(view -> playSequence());

        final Button saveBtn = findViewById(R.id.buttonConfigSave);
        saveBtn.setOnClickListener(view -> saveSequence());

        final ImageView rmBtn = findViewById(R.id.imageConfigRm);
        rmBtn.setOnClickListener(view -> rmLastFromSequence());
    }

    /**
     * Add a new character to the sequence
     * @param tv The TextView which triggered this function (and contains the character as text)
     */
    private void addToSequence(final TextView tv) {
        String btnText = tv.getText().toString();
        sequence += btnText.equals("BAT") ? "M" : btnText;
        passwdView.setText(sequence);
    }

    /**
     * Removes the last character from the current sequence.
     */
    private void rmLastFromSequence() {
        if ( sequence.length() > 0 ) {
            sequence = sequence.substring(0, sequence.length()-1);
            passwdView.setText(sequence);
        }
    }

    /**
     * Store the current sequence to the properties file. It will also load the sequence
     * again and compare it to the internal sequence variable and a toast message will indicate success.
     */
    private void saveSequence() {
        final String currentSeq = sequence;
        ConfigHandler.setProperty(this, sequence);
        loadSequence();

        if ( currentSeq.equals(sequence) ) {
            Toast.makeText(
                this,
                sequence + " successfully saved",
                Toast.LENGTH_LONG
            ).show();
            this.finish();
        } else {
            Toast.makeText(
                    this,
                    "Failed to save: " + sequence,
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    /**
     * Load the sequence from the properties file and update the internal sequence variable.
     */
    private void loadSequence() {
        sequence = ConfigHandler.getProperty(this, ConfigHandler.getPasswordPattern());
        passwdView.setText(sequence);
    }

    /**
     * Highlight the TextViews in order of the current sequence.
     */
    private void playSequence() {
        int cnt = 0;
        final Handler handler = new Handler();

        for ( char x : sequence.toCharArray() ) {
            AnimationDrawable drawable = (AnimationDrawable) charMap.get(x);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawable.start();
                }
            }, (cnt++)*(ANIMATION_DURATION+500));
        }
    }

    /**
     * Initialize the charMap, which maps characters (A-M) to the corresponding drawable background
     * (used for highlighting animation).
     */
    private void initCharMap() {
        AnimationDrawable drawable;

        for ( int i=0; i<charList.length; i++ ) {
            drawable = new AnimationDrawable();

            drawable.addFrame(new ColorDrawable(Color.TRANSPARENT), 0);
            drawable.addFrame(new ColorDrawable(getResources().getColor(R.color.highLight)), ANIMATION_DURATION);
            drawable.addFrame(new ColorDrawable(Color.TRANSPARENT), 0);
            drawable.setEnterFadeDuration(FADE_IN_DURATION);
            drawable.setExitFadeDuration(FADE_OUT_DURATION);
            drawable.setOneShot(true);

            tvList[i].setBackground(drawable);

            charMap.put(charList[i], drawable);
        }
    }
}
