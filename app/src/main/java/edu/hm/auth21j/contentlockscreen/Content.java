package edu.hm.auth21j.contentlockscreen;

public enum Content {
    HOUR_1("A"),
    HOUR_2("B"),
    MINUTE_1("C"),
    MINUTE_2("D"),
    DAY_1("E"),
    DAY_2("F"),
    MONTH_1("G"),
    MONTH_2("H"),
    YEAHR_1("I"),
    YEAHR_2("J"),
    YEAHR_3("K"),
    YEAHR_4("L"),
    AKKU("M");

    private String token;

    Content(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
