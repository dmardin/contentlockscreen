package edu.hm.auth21j.contentlockscreen;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ContentParser {

    public static String buildPassword(MainActivity mainActivity, String pattern) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        String time = sdf.format(calendar.getTime());
        sdf = new SimpleDateFormat("ddMMyyyy");
        String date = sdf.format(calendar.getTime());
        StringBuilder pw = new StringBuilder();
        for (char c : pattern.toCharArray()) {
            if (String.valueOf(c).equals(Content.HOUR_1.getToken())) {
                pw.append(time.charAt(0));
            } else if (String.valueOf(c).equals(Content.HOUR_2.getToken())) {
                pw.append(time.charAt(1));
            } else if (String.valueOf(c).equals(Content.MINUTE_1.getToken())) {
                pw.append(time.charAt(2));
            } else if (String.valueOf(c).equals(Content.MINUTE_2.getToken())) {
                pw.append(time.charAt(3));
            } else if (String.valueOf(c).equals(Content.DAY_1.getToken())) {
                pw.append(date.charAt(0));
            } else if (String.valueOf(c).equals(Content.DAY_2.getToken())) {
                pw.append(date.charAt(1));
            } else if (String.valueOf(c).equals(Content.MONTH_1.getToken())) {
                pw.append(date.charAt(2));
            } else if (String.valueOf(c).equals(Content.MONTH_2.getToken())) {
                pw.append(date.charAt(3));
            } else if (String.valueOf(c).equals(Content.YEAHR_1.getToken())) {
                pw.append(date.charAt(4));
            } else if (String.valueOf(c).equals(Content.YEAHR_2.getToken())) {
                pw.append(date.charAt(5));
            } else if (String.valueOf(c).equals(Content.YEAHR_3.getToken())) {
                pw.append(date.charAt(6));
            } else if (String.valueOf(c).equals(Content.YEAHR_4.getToken())) {
                pw.append(date.charAt(7));
            } else if (String.valueOf(c).equals(Content.AKKU.getToken())) {
                IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                Intent batteryStatus = mainActivity.getApplicationContext().registerReceiver(null, intentFilter);
                int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                pw.append(String.valueOf(level));
            } else {
                pw.append("");
            }
        }

        return pw.toString();
    }
}
