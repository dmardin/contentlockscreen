package edu.hm.auth21j.contentlockscreen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateTimeAndDate();
        initializeListener();
    }

    private void updateTimeAndDate() {
        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!this.isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final TextView textTime = findViewById(R.id.textTime);
                                final TextView textDate = findViewById(R.id.textDate);
                                Calendar calendar = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                                textTime.setText(sdf.format(calendar.getTime()));

                                sdf = new SimpleDateFormat("dd.MM.yyyy");
                                textDate.setText(sdf.format(calendar.getTime()));
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        thread.start();
    }

    private void initializeListener() {
        final TextView textInput = findViewById(R.id.textInput);
        final Button buttonOne = findViewById(R.id.buttonOne);
        final Button buttonTwo = findViewById(R.id.buttonTwo);
        final Button buttonThree = findViewById(R.id.buttonThree);
        final Button buttonFour = findViewById(R.id.buttonFour);
        final Button buttonFive = findViewById(R.id.buttonFive);
        final Button buttonSix = findViewById(R.id.buttonSix);
        final Button buttonSeven = findViewById(R.id.buttonSeven);
        final Button buttonEight = findViewById(R.id.buttonEight);
        final Button buttonNine = findViewById(R.id.buttonNine);
        final Button buttonZero = findViewById(R.id.buttonZero);
        final ImageView imageDelete = findViewById(R.id.imageDelete);
        final ImageView imageCheck = findViewById(R.id.imageCheck);

        buttonOne.setOnClickListener(view -> textInput.append("1"));
        buttonTwo.setOnClickListener(view -> textInput.append("2"));
        buttonThree.setOnClickListener(view -> textInput.append("3"));
        buttonFour.setOnClickListener(view -> textInput.append("4"));
        buttonFive.setOnClickListener(view -> textInput.append("5"));
        buttonSix.setOnClickListener(view -> textInput.append("6"));
        buttonSeven.setOnClickListener(view -> textInput.append("7"));
        buttonEight.setOnClickListener(view -> textInput.append("8"));
        buttonNine.setOnClickListener(view -> textInput.append("9"));
        buttonZero.setOnClickListener(view -> textInput.append("0"));

        imageDelete.setOnClickListener(view -> {
            String text = textInput.getText().toString();
            if (text.length() > 0) {
                textInput.setText(text.substring(0, text.length() - 1));
            }
        });

        imageCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pattern = ConfigHandler.getProperty(MainActivity.this, ConfigHandler.getPasswordPattern());
                String pw = ContentParser.buildPassword(MainActivity.this, pattern);
                if (textInput.getText().toString().equals(pw)) {
                    //Toast.makeText(MainActivity.this, "Richtig: " + textInput.getText() + " | " + pw, Toast.LENGTH_SHORT).show();
                    MainActivity.this.finish();
                    //System.exit(0);
                }/* else {
                    Toast.makeText(MainActivity.this, "Falsch: " + textInput.getText() + " | " + pw, Toast.LENGTH_SHORT).show();
                }*/
                textInput.setText(null);
            }
        });
    }
}
