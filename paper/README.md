# Paper on Content-based Authentication

## Based on the SIGCHI template

The original template is `old/proceedings.tex`. It renders like `old/LaTeX SIGCHI Proceedings Format.pdf`.
The original template has been separated into individual files for each section.
These files are located in the subdirectory `sections` and included by `main.tex`.

## Build the document (Linux)

Just run `./build.sh`.  
This script expects `xelatex`, `xreader` and `bibtex` as well as several texlive packages to be installed!

## Build the document (General)

`main.tex` - as the name suggests - is the main document. Build this with your preferred Latex tool and all other files (e.g. the sections) will be included automatically.