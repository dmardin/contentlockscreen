#!/bin/bash

RED='\033[1;31m' # Red font
GRN='\033[1;32m' # Green font
NC='\033[0m' # No color
BUILD_DIR='output'
MAIN='main'

mkdir -p $BUILD_DIR;
xelatex -output-directory=./$BUILD_DIR/ -halt-on-error $MAIN && \
bibtex $BUILD_DIR/$MAIN && \
xelatex -output-directory=./$BUILD_DIR/ -halt-on-error $MAIN && \
(echo -e "\n${GRN}SUCCESS${NC}\n"; xreader output/$MAIN.pdf &) || echo -e "\n${RED}!!! FAILED !!!${NC}\n"
